Update & Backup
***************

Update v3.a.b > v3.x.y
--------------------------

1. Connect to your server via SSH


2. Create a backup of the current version using the following command:
    
    ``sudo cp -pr cs cs_backup``

3.  Change to the cs directory
    
    ``cd ./cs``

4.  Download the latest Connectivity Suite console application. The credentials for repository are provided by NetModule.
    
    ``curl -u cs-install ftp://ftp.netmodule.com/latest/cs-cmd -o cs-cmd``

5.  Run the cs-cmd tool and select "update assistant"
    
    ``./cs-cmd``


Upgrade v2.6 > v3.0
--------------------------

Since v3.0 is based on a different software architecture than v2.6, this is not only an update but also a migration.

.. warning:: In case any of the following steps fail we advice to **snapshot the Virtual Machine** where the Connectivity Suite is installed so that a roll back is possible.

.. warning:: To install and run the Connectivty Suite v3 you need to have an **up to date Ubuntu 20/22/24 or Debian 10/11/12** with **Docker v20+** and the **Docker Compose v2+** plugin installed.
  See :numref:`Server Software` for details.

.. warning:: Routers with **NRSW older than 4.3** will not be able to connect to a Connectivity Suite v3.x. Make sure to update the outdate Router Software before upgrading the Connectivity Suite.

.. note:: The following steps assume that you have installed your CS v2.6 in your home directory under ``~/cs``. If this is not the case, please adjust the paths below accordingly.


1. Connect to your server via SSH
2. Download the Connectivity Suite Backup script
   
   ``curl https://repo.netmodule.com/repository/cs/v2.6/scripts/export_cs_v2.py -o export_cs_v2.py``
3. Export Connectivity Suite v2.6 data with the Backup script and note the location of the cs_export.zip file. You will need this later.
   
   ``python ./export_cs_v2.py``

4. Change to the *~/cs/docker-prod* directory
   
   ``cd ./cs/docker-prod``
5. Stop the v2.6 instance
   
   ``docker-compose down``
6.  Change directory back to home
    
    ``cd ~``
7.  Rename the cs directory to cs_v2x
    
    ``mv cs cs_v2x``
8.  Create new cs directory
    
    ``mkdir cs``
9.  Change to new cs directory
    
    ``cd ./cs``
10. Run a ls command to make sure the directory is empty
    
    ``ls``
11. Download Connectivity Suite console application. The credentials for repository are provided by NetModule.
    
    ``curl -u cs-install ftp://ftp.netmodule.com/latest/cs-cmd -o cs-cmd``
12. Add execution permission to newly downloaded cs-cmd
    
    ``sudo chmod +x ./cs-cmd``
13. Run the cs-cmd tool and select "v2.x migration assistant"
    
    ``./cs-cmd``
14. The requested path to the cs_export.zip was printed out at step 3 and is similar to
    
    ``/home/ubuntu/cs_export.zip``
15. Optional: Remove v2.6
    
    ``rm -rf ~/cs_v2x``

.. warning:: When updating from v2.6 to v3.0, certain restrictions must be observed which are noted in chapter :numref:`Migration Notes`

Migration Notes
=================

The migration to v3.0 will not migrate the full data of v2.6. The following list shows the affected entities:

* Replacement Devices
  
  Devices which should replace an existing device and their replacement provisioning config will not be migrated. Make sure to finish all your device replacement jobs before you upgrade.

* Provisioning Devices
  
  Devices currently connected to the provisioning network will **not be migrated**. Make sure to move all devices to their tenant before you upgrade.

* Logs
  
  The plain-text logs will no be migrated.

* Jobs
  
  Jobs will not be migrated. Make sure that there are no open jobs before you migrate.

* Provisioning Configurations
  
  Already downloaded configurations from CS v2.6 will no longer work for CS v3.0.

* Device metric data
  
  Up to 6 months of online/offline device state changes are migrated, **but only up to a maximum of 4'000'000 state changes in total, depending on which limit is reached first**.
  