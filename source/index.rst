.. Connectivity Suite documentation master file, created by
   sphinx-quickstart on Tue Sep  3 08:04:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. .. role:: raw-html(raw)
  .. :format: html



.. figure:: images/welcomebanner.png
  :name: welcomebanner
  :align: left

..

CS Manual Version 3.7
=======================================

| **Copyright:**  2024 © NetModule AG
| **Modification date:**  06.12.2024
| **Legal:**  :ref:`Legal Aspects`
| 

Overview
------------------------------------------------------------

+-------------------------------+--------------------------------+----------------------------------+
| **Installation**              | **Operation**                  | **Maintenance**                  |
+-------------------------------+--------------------------------+----------------------------------+
| :ref:`Installation`           | :ref:`User Management`         | :ref:`Support & Troubleshooting` |
+-------------------------------+--------------------------------+----------------------------------+
| :ref:`First Steps`            | :ref:`Device Management`       | :ref:`Platform Status`           |
+-------------------------------+--------------------------------+----------------------------------+
| :ref:`Network architecture`   | :ref:`Dashboard`               | :ref:`Platform Updates`          |
+-------------------------------+--------------------------------+----------------------------------+
| :ref:`Network Management`     | :ref:`Variables & Bulk Editor` | :ref:`Update & Backup`           |
+-------------------------------+--------------------------------+----------------------------------+
| :ref:`Certificate Management` | :ref:`Artifacts`               |                                  |
+-------------------------------+--------------------------------+----------------------------------+
|                               | :ref:`Jobs`                    |                                  |
+-------------------------------+--------------------------------+----------------------------------+
|                               | :ref:`Remote Access`           |                                  |
+-------------------------------+--------------------------------+----------------------------------+
|                               | :ref:`Child Device Access`     |                                  |
+-------------------------------+--------------------------------+----------------------------------+
|                               | :ref:`User Traffic`            |                                  |
+-------------------------------+--------------------------------+----------------------------------+

**Content**

.. toctree::
    :maxdepth: 3
    :numbered:

    introduction
    installation
    first_steps
    network_management
    certificate_management
    device_management
    dashboard
    variables
    artifacts
    jobs
    remote_access
    network_architecture
    child_devices
    user_traffic
    user_management
    customisations
    troubleshooting
    platform_status
    platform_updates
    update
    appendix


| **NetModule AG**
| Maulbeerstrasse 10
| 3011 Bern
| Switzerland
| info@netmodule.com
| Tel +41 31 985 25 10
| Fax +41 31 985 25 11
|
| https://www.netmodule.com
