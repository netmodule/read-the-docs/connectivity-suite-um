Introduction
*********************************

Key Features
=====================================

**Remote access with one click:**
Access all connected routers and clients of your routers within seconds via the web interface of the Connectivity Suite.

**Configure & Control:**
Remote execute automated and scheduled remote over-the-air updates and configurations.

**Communication via REST API:**
REST API to access functionalities from the Connectivity Suite via your own platform or application.

**Scalable network:**
Adding new assets to your existing network can be done within a few minutes and without network configuration know-how required.

**Organize & Control Access:**
Grouping assets based on a multitenancy capability with hierarchical access control to group your assets according to customers, regions, teams etc.

**End-to-end Security:**
Auto-setup of encrypted VPN infrastructure for an end-to-end encryption to secure your communication and your network without any user intervention required.

**Cloud based or On-Premise installation:**
Installation and operation is possible in a cloud environment but also on-premise on your in-house servers to keep control of your data.

System Architecture
=============================================

.. figure:: images/systemarchitecture.png
    :width: 400px
    :name: systemarchitecture

    System architecture
..


The Connectivity Suite is not only a software but also a network infrastructure. This means that when installing the software, you install a network infrastructure that must be integrated into your existing network.
When the software is installed, the Connectivity Suite automatically sets up an OpenVPN network. Whenever a Device is connected with the Connectivity Suite this Device will be integrated in the OpenVPN Network of the Connectivity Suite.
The :numref:`systemarchitecture` shows a simplified representation of the Connectivity Suite infrastructure.

Web interface Connectivity Suite
================================================

The web interface of the Connectivity Suite provides the interface to configure and setup the Connectivity Suite incl. the Devices. Find below a description and general overview of the web interface.

.. figure:: images/ui_overview.png
    :width: 1000px
    :name: UI_overview

    UI overview
..


+--------------------------------------------+------------------------------------------+-----------------------------------+------------------------------------+
| **1** - Main menu                          | **2** - Main Dialogue                    | **3** - Detail Dialogue           | **4** - Top menu                   |
+--------------------------------------------+------------------------------------------+-----------------------------------+------------------------------------+
| The main menu gives access to the main     | Shows detailed lists of devices,         | Provides details of the           | The top menu gives access the      |
| features of the CS which are:              | artifacts, network and jobs,             | selected element in the Main      | the following features:            |
|                                            | depending on chosen element in the       | dialogue.                         |                                    |
| * Dashboard                                | Main menu.                               |                                   | * Help                             |
| * Devices                                  |                                          |                                   |                                    |
| * Artifacts                                |                                          |                                   |   * User Manual                    |
|                                            |                                          |                                   |   * NetModule Wiki                 |
|   * Configuration                          |                                          |                                   |   * NetModule Support              |
|   * Snippets                               |                                          |                                   |   * API Documentation              |
|   * Software                               |                                          |                                   |   * About                          |
|   * Provisioning                           |                                          |                                   |                                    |
|                                            |                                          |                                   | * Settings                         |
| * Network                                  |                                          |                                   |                                    |
| * Jobs                                     |                                          |                                   |   * Manage Users                   |
|                                            |                                          |                                   |   * Download Logs                  |
|                                            |                                          |                                   |                                    |
|                                            |                                          |                                   | * Notifications                    |
|                                            |                                          |                                   | * User                             |
|                                            |                                          |                                   |                                    |
|                                            |                                          |                                   |   * Change Password                |
|                                            |                                          |                                   |   * Log out                        |
+--------------------------------------------+------------------------------------------+-----------------------------------+------------------------------------+
