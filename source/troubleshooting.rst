Support & Troubleshooting
*********************************

Support
========================

On the **Support** page, all functionality regarding troubleshooting the platform is located. The page has four main-tabs:

Belden Support
-------------------------------------

This tab provides a link to the official Belden Support form to contact us regarding platform issues.

It also provides a link to download a platform techsupport file which you are asked to attach in case you contact us through the contact form.

Logs
-------------------------------------

This tab shows the platform logs and makes them available for download.

Network
-------------------------------------

To troubleshoot networking issues, this tab contains several tools:

* Ping
* Trace Route
* MTU Discovery

Data Maintenance
-------------------------------------

In order to cleanup obsolete data and ensure a good performance of the platform, this tab provides you with several cleanup tasks:

+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Task                           | Description                                                                                                                                                                    |
+================================+================================================================================================================================================================================+
| Platform Logs                  | If checked, deletes all platform logs up-until the specified date and up to the specified log-level                                                                            |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Platform Events                | If checked, deletes all events up-until the specified date                                                                                                                     |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Hard Delete                    | If checked, deletes all as deleted marked entities up-until the specified date                                                                                                 |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Certificates                   | If checked, deletes all obsolete certificates up-until the specified date. Certificates still in use are not deleted.                                                          |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Jobs                           | If checked, deletes all jobs up-until the specified date. Optionally, this can be limited to system jobs only.                                                                 |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Device States                  | If checked, deletes all device state data up-until the specified date. Optionally, this can be limited to a specific device. Otherwise it is applied on all devices (default). |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Device Infos                   | If checked, deletes all device info data up-until the specified date. Optionally, this can be limited to a specific device. Otherwise it is applied on all devices (default).  |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Specific Device Configurations | If checked, deletes all Specific Device Configurations except the latest n configurations specified by **Keep Revisions**                                                      |
+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. warning:: Since these cleanup tasks cannot be undone, we recommend to create a snapshot beforehand.

.. warning:: Data Maintenance could use quite some system resources depending on how much data has been accumulated. Please ensure, that this task is performed during a maintenance window.

.. warning:: On low-end systems we have seen cases where the complete platform got frozen during Data Maintenance. This is a sign of too few system resources. Consider adding more RAM and/or CPU cores.

Troubleshooting
========================

Troubleshoot a Device
-------------------------------------

If there are problems with a Device, there are several things that you can check:

* Is the Device online?

  * No: Check when it was last online by navigating to the Health page and selecting the specific Device

    * Is the Device powered on?

        * Yes: Try to access the Device directly by downloading the Service Access VPN configuration and establishing a VPN connection to the Core Network
        * No: Power on the Device

* Yes, do a direct Device access by first downloading the Service Access VPN configuration and troubleshooting the Device outside of the Connectivity Suite

Troubleshoot the Connectivity Suite
-------------------------------------

To download the platform techsupport file, an account with **Platform Administrator** rights is required.

Download support file < v3.6.3:

1. Click on the wrench icon in the upper right corner
2. Select **Logs**
3. Click on **Download Support File**. The default options visible in the dialog usually work. In case you are running into performance issues, you might want to reduce the entry limit from 100'000 to 1000.
4. Download the file and send it to the Connectivity Suite support

Download support file >= v3.6.3:

1. Click on the wrench icon in the upper right corner
2. Select **Support**
3. Click on **here** under the **Generate a support file** title. The default options visible in the dialog usually work. In case you are running into performance issues, you might want to reduce the entry limit from 100'000 to 1000.
4. Download the file and send it to the Connectivity Suite support

inotify limit reached
-------------------------------------

If you are running more than 50 VPN Networks/Servers, you might run into the following error in the log:

.. code-block:: text

  The configured user limit (128) on the number of inotify instances has been reached, or the per-process limit on the number of open file descriptors has been reached.

To resolve this issue, we recommend to increase the ``fs.inotify.max_user_instances`` limit to 512. You can do this in the console with ``echo fs.inotify.max_user_instances=512 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p``.

.. _browser_settings:

Browser settings Connectivity Suite SSL Certificate
---------------------------------------------------

Downloading the Connectivity Suite Root CA certificate using Microsoft Edge

1. Click on Certificate Error to the left of the address bar
2. Choose the Connectivity Suite Root CA from the menu on the right
3. Click on Export to file, this is the file that needs to be imported using one of the import procedures described below

**Making your browser trust the Connectivity Suite Root CA**

The client operating system or browser now needs to have the CA certificate added to its list of trusted CAs. The instructions vary by operating system and browser but instructions for a few major browsers are listed below.

+------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Client (Operating System, Browser) | Instructions                                                                                                                                                                                        |
+====================================+=====================================================================================================================================================================================================+
| Microsoft Windows                  | Right click the CA certificate file and select 'Install Certificate'.                                                                                                                               |
|                                    | Follow the prompts to add the certificate to the trust store either                                                                                                                                 |
|                                    | for the current user only or all users of the computer.                                                                                                                                             |
+------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Firefox                            | Firefox does not use the operating systems trust store so the CA needs                                                                                                                              |
|                                    | to be added manually. Manually add certificates and manage added certificates                                                                                                                       |
|                                    | through Firefox's Privacy & Security preferences. More information can be found on `Mozilla Wiki <https://wiki.mozilla.org/PSM:Changing_Trust_Settings#Trusting_an_Additional_Root_Certificate>`_   |
+------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Chrome                             | If Chrome is configured to use the Windows trust store,                                                                                                                                             |
|                                    | adding the certificate to Windows (see above) is sufficient                                                                                                                                         |
|                                    | to add trust to the browser as well. Otherwise the certificate                                                                                                                                      |
|                                    | can be added manually using the following procedure:                                                                                                                                                |
|                                    | Open Settings > Advanced > Manage Certificates > Authorities, and select Import.                                                                                                                    |
+------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Edge/Internet Explorer             | Edge/Internet Explorer uses the Windows trust store so adding the certificate to Windows                                                                                                            |
|                                    | (see above) is sufficient to add trust to the browser as well.                                                                                                                                      |
+------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Docker log rotation
-------------------------------------

**By default, Docker does not rotate logs!**  To prevent the server's disk from running out of space over time due to huge Docker logs you have to do the following:  

1. Create the file */etc/docker/daemon.json* if it does not already exist
2. Add the following content to */etc/docker/daemon.json*
   
.. code-block:: text
  
  {
    "log-driver": "local",
    "log-opts": {
      "max-size": "20m",
      "max-file": "5"
    }
  }

In general, we recommend using the values provided in the example for *max-size* and *max-file*. If you are more experienced, you can consider choosing a different logging driver and send your Docker logs to e.g. Splunk or any other supported logs collector (see https://docs.docker.com/engine/logging/configure/#supported-logging-drivers).

3. Restart the Docker service: ``service docker restart``

4. Navigate into the folder where the Connectivity Suite installation is located (e.g. ``cd ~/cs/app``)

5. Shut down the Connectivity Suite: ``docker compose down``

6. Start the Connectivity Suite again: ``docker compose up -d``