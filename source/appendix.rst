
Appendix
******************************************************

Abbreviations
===============================================
+------+-------------------------------------+
| API  | Application   Programming Interface |
+------+-------------------------------------+
| AWS  | Amazon   Web Services               |
+------+-------------------------------------+
| CLI  | Command   Line interface            |
+------+-------------------------------------+
| CS   | Connectivity Suite                  |
+------+-------------------------------------+
| FQDN | Fully   Qualified Domain Name       |
+------+-------------------------------------+
| HMI  | Human   Machine Interface           |
+------+-------------------------------------+
| NM   | NetModule                           |
+------+-------------------------------------+
| OTA  | Over   the Air                      |
+------+-------------------------------------+
| REST | Representational   State Transfer   |
+------+-------------------------------------+
| SSH  | Secure   Shell                      |
+------+-------------------------------------+
| UI   | User   interface                    |
+------+-------------------------------------+
| VPN  | Virtual   Private Network           |
+------+-------------------------------------+


Glossary
===============================================

+----------------------+----------------------+----------------------+
| **v3**               | **v2.x**             | **Meaning in v3.x**  |
+======================+======================+======================+
|                      | Tenant Network,      | Name of a VPN subnet |
|                      | Tenant-internal      | consisting of        |
|                      | Network              | Devices and          |
|                      |                      | Devices.             |
+----------------------+----------------------+----------------------+
| External Address     |                      | The address of a     |
|                      |                      | Device under which   |
|                      |                      | it’s available       |
|                      |                      | without the VPN      |
|                      |                      | infrastructure       |
+----------------------+----------------------+----------------------+
| (Parent) Device      | Device               | Anything that is     |
|                      |                      | connected to the     |
|                      |                      | Connectivity Suite   |
|                      |                      | this can be router,  |
|                      |                      | switch, access point |
|                      |                      | etc. Whereas a       |
|                      |                      | Parent Device is a   |
|                      |                      | Device that is       |
|                      |                      | directly connected   |
|                      |                      | to the Connectivity  |
|                      |                      | Suite e.g. router.   |
+----------------------+----------------------+----------------------+
| Alias                | Shortname            |                      |
+----------------------+----------------------+----------------------+
| Artifact             |                      | An artifact can be a |
|                      |                      | SDK-script,          |
|                      |                      | LXC-container,       |
|                      |                      | Landing page,        |
|                      |                      | user-configuration,  |
|                      |                      | License NetModule    |
|                      |                      | router software,     |
|                      |                      | Firmware modem.      |
+----------------------+----------------------+----------------------+
| Backend Network      | Home Network         | The VPN network that |
|                      |                      | connects the API and |
|                      |                      | the VPN servers      |
+----------------------+----------------------+----------------------+
| Backend VPN Server   | Home Server          | A VPN server which   |
|                      |                      | defines the Backend  |
|                      |                      | Network              |
+----------------------+----------------------+----------------------+
| Child Device         | End Device,          | Devices connected to |
|                      | Connected Device,    | another Device.      |
|                      | Managed Devices,     | E.g., a switch       |
|                      | Client Devices       | connected to a       |
|                      |                      | router.              |
+----------------------+----------------------+----------------------+
| Connectivity Suite   |                      | The infrastructure   |
| infrastructure       |                      | of the Connectivity  |
|                      |                      | Suite that includes  |
|                      |                      | the software         |
|                      |                      | instance itself and  |
|                      |                      | the networks w/o any |
|                      |                      | Devices.             |
+----------------------+----------------------+----------------------+
| Connectivity Suite   |                      | Name for all         |
| Networks             |                      | networks created by  |
|                      |                      | the Connectivity     |
|                      |                      | Suite (Home, Prov.   |
|                      |                      | etc.).               |
+----------------------+----------------------+----------------------+
| Container            | LXC Container        |                      |
+----------------------+----------------------+----------------------+
| Core Address Block   | Core Network         | The Core Address     |
|                      |                      | Block is used for    |
|                      |                      | Docker Networks, the |
|                      |                      | Provisioning         |
|                      |                      | Network, and the     |
|                      |                      | Backend Network.     |
+----------------------+----------------------+----------------------+
| Device Access Routes |                      | Routes that are      |
|                      |                      | added using an       |
|                      |                      | iroute and push      |
|                      |                      | route via the CCD    |
|                      |                      | and OpenVPN server   |
|                      |                      | configuration.       |
+----------------------+----------------------+----------------------+
| Device Configuration | Configuration        | The Configuration is |
|                      |                      | always bound to a    |
|                      |                      | specific physical    |
|                      |                      | Device. It contains  |
|                      |                      | Device-specific      |
|                      |                      | information like VPN |
|                      |                      | certificates and SSH |
|                      |                      | keys, therefore it   |
|                      |                      | cannot be deployed   |
|                      |                      | to any other Device. |
+----------------------+----------------------+----------------------+
| Device Information   |                      | Information which    |
|                      |                      | describes a Devices  |
|                      |                      | such as firmware,    |
|                      |                      | IP-address,          |
|                      |                      | MAC-address etc.     |
+----------------------+----------------------+----------------------+
| Device information   |                      | The information the  |
|                      |                      | Connectivity Suite   |
|                      |                      | queries from the     |
|                      |                      | Devices.             |
+----------------------+----------------------+----------------------+
| Device Health        |                      | Information which    |
|                      |                      | describes constantly |
|                      |                      | changing values of a |
|                      |                      | Device such as       |
|                      |                      | location, bandwidth, |
|                      |                      | temperature etc.     |
+----------------------+----------------------+----------------------+
| Device Model         | Model                | Specific product     |
|                      |                      | like NB800 by        |
|                      |                      | NetModule            |
+----------------------+----------------------+----------------------+
| Device specific      |                      | Configuration        |
| configuration        |                      | parameters that are  |
| parameters           |                      | valid for only one   |
|                      |                      | specific Device.     |
+----------------------+----------------------+----------------------+
| Device Status        |                      | Information which    |
|                      |                      | describes the status |
|                      |                      | of a Device such as  |
|                      |                      | online/offline and   |
|                      |                      | inactive/active      |
+----------------------+----------------------+----------------------+
| Device Type          | Device Type          | Product category     |
|                      |                      | like Router, vending |
|                      |                      | machine, switch,     |
|                      |                      | access point etc.    |
+----------------------+----------------------+----------------------+
| Device VPN Address   |                      | The subnet allocated |
| Block                |                      | to a specific Device |
|                      |                      | inside a VPN         |
|                      |                      | Network.             |
+----------------------+----------------------+----------------------+
| End Device           |                      | Any Device that is   |
|                      |                      | connected to a       |
|                      |                      | network Device such  |
|                      |                      | as a camera, video   |
|                      |                      | system, passenger    |
|                      |                      | counter, printer     |
|                      |                      | etc.                 |
+----------------------+----------------------+----------------------+
| Firmware             | Software,            |                      |
|                      | Firmware,            |                      |
|                      | Application          |                      |
+----------------------+----------------------+----------------------+
| Generic              |                      | Configuration        |
| configuration        |                      | parameters that are  |
| parameters           |                      | valid for more than  |
|                      |                      | one Device.          |
+----------------------+----------------------+----------------------+
| Generic Device       | Generic Device       | Any device that is   |
|                      |                      | only defined by an   |
|                      |                      | IP address and can   |
|                      |                      | be pinged by the     |
|                      |                      | Connectivity Suite.  |
|                      |                      | The device cannot be |
|                      |                      | managed via the      |
|                      |                      | Connectivity Suite.  |
+----------------------+----------------------+----------------------+
| Job                  | Job                  | Can be used to       |
|                      |                      | execute time-based   |
|                      |                      | actions on Devices.  |
|                      |                      | The execution of a   |
|                      |                      | Job can be flexibly  |
|                      |                      | scheduled (e.g.      |
|                      |                      | firmware or          |
|                      |                      | configuration update |
|                      |                      | for a group of       |
|                      |                      | devices).            |
+----------------------+----------------------+----------------------+
| LAN                  |                      | Local Area Network   |
|                      |                      | behind the router.   |
+----------------------+----------------------+----------------------+
| Lan Address          | IP in LAN            | The IP/Address of a  |
|                      |                      | Device behind a      |
|                      |                      | router, e.g., the IP |
|                      |                      | of a Access          |
|                      |                      | Point/Switch.        |
+----------------------+----------------------+----------------------+
| Managed Devices      |                      | It distinguishes     |
|                      |                      | Devices that are     |
|                      |                      | managed by the       |
|                      |                      | Connectivity Suite   |
|                      |                      | and Devices that are |
|                      |                      | not. Example a       |
|                      |                      | NetModule switch is  |
|                      |                      | a Child Device,      |
|                      |                      | however you can not  |
|                      |                      | compare it to an End |
|                      |                      | Device or even a     |
|                      |                      | switch that is not   |
|                      |                      | managed by the       |
|                      |                      | Connectivity Suite.  |
+----------------------+----------------------+----------------------+
| Module               |                      | A module is hardware |
|                      |                      | module which is      |
|                      |                      | installed on the     |
|                      |                      | router and part of a |
|                      |                      | modem that contains  |
|                      |                      | its own firmware     |
|                      |                      | such as a GNSS       |
|                      |                      | module, WLAN module  |
|                      |                      | etc.                 |
+----------------------+----------------------+----------------------+
| Open VPN Server      | Tenant               | An Open VPN Server   |
|                      |                      | which is needed for  |
|                      |                      | the VPN Network.     |
+----------------------+----------------------+----------------------+
| Platform Address     | IP Address in Home   | The platform wide    |
|                      | Network,             | unique address of a  |
|                      | IP in Home           | Device               |
+----------------------+----------------------+----------------------+
| Platform Address     | Home Network         | A VPN subnet         |
| Block                |                      | consisting of all    |
|                      |                      | VPN Networks. A      |
|                      |                      | Connectivity Suite   |
|                      |                      | instance has exactly |
|                      |                      | one Platform Address |
|                      |                      | Block.               |
+----------------------+----------------------+----------------------+
| Provisioning         | Provisioning         | The Provisioning     |
| Configuration        | Configuration        | Configuration is not |
|                      |                      | bound to a specific  |
|                      |                      | physical Device. It  |
|                      |                      | contains no          |
|                      |                      | Device-specific      |
|                      |                      | information;         |
|                      |                      | therefore, it can be |
|                      |                      | deployed to any      |
|                      |                      | other Device of the  |
|                      |                      | corresponding Device |
|                      |                      | model.               |
+----------------------+----------------------+----------------------+
| Provisioning Network | Provisioning Server  | Newly added Devices  |
|                      |                      | will be connected to |
|                      |                      | the Provisioning     |
|                      |                      | Network until a user |
|                      |                      | moves those Devices  |
|                      |                      | to a VPN Network.    |
+----------------------+----------------------+----------------------+
| Provisioning VPN     | Provisioning Server  | A VPN server which   |
| Server               |                      | defines the          |
|                      |                      | Provisioning         |
|                      |                      | Network.             |
+----------------------+----------------------+----------------------+
| Readable             | Configuration        | A Readable           |
| Configuration        |                      | Configuration        |
| (internal use only)  |                      | represents the       |
|                      |                      | content of the       |
|                      |                      | user-config.cfg      |
|                      |                      | file. In comparison  |
|                      |                      | with a Snippet, a    |
|                      |                      | Snippet ist a part   |
|                      |                      | of the Readable      |
|                      |                      | Configuration.       |
+----------------------+----------------------+----------------------+
| Repository           | Files                | The collection of    |
|                      |                      | deployable           |
|                      |                      | artifacts.           |
+----------------------+----------------------+----------------------+
| Script               | SDK Script           | Script which can be  |
|                      |                      | uploaded to a Device |
|                      |                      | which could be a     |
|                      |                      | bash script, shell   |
|                      |                      | script, SDK script   |
|                      |                      | etc.                 |
+----------------------+----------------------+----------------------+
| Service Access       | Service Access       | The Service Access   |
|                      |                      | is the established   |
|                      |                      | VPN tunnel to        |
|                      |                      | directly access the  |
|                      |                      | Parent Devices and   |
|                      |                      | their Child Devices. |
|                      |                      | Service Access       |
|                      |                      | connections can be   |
|                      |                      | established to the   |
|                      |                      | Backend VPN-,        |
|                      |                      | Provisioning VPN or  |
|                      |                      | VPN-server           |
+----------------------+----------------------+----------------------+
| Snippet              | Snippet              | Refers to a subset   |
|                      |                      | of configuration     |
|                      |                      | parameters taken     |
|                      |                      | from a configuration |
|                      |                      | file. Snippets can   |
|                      |                      | be used to execute   |
|                      |                      | bulk configuration   |
|                      |                      | of Devices without   |
|                      |                      | overwriting          |
|                      |                      | Device-specific      |
|                      |                      | configuration        |
|                      |                      | parameters. Snippets |
|                      |                      | configure only       |
|                      |                      | Device specific      |
|                      |                      | features. A Snippet  |
|                      |                      | can consist of one   |
|                      |                      | or more              |
|                      |                      | configuration        |
|                      |                      | parameters.          |
+----------------------+----------------------+----------------------+
| Task                 | Task                 | A Job can contain    |
|                      |                      | multiple Tasks. A    |
|                      |                      | Task is the          |
|                      |                      | execution of an      |
|                      |                      | action per Device.   |
+----------------------+----------------------+----------------------+
| VPN Address          | IP Address in Tenant | The IP assigned by a |
|                      | Network,             | VPN Server to a      |
|                      | IP in Tenant,        | router               |
|                      | Tenant-internal IP   |                      |
+----------------------+----------------------+----------------------+
| VPN Network          | Tenant,              | A VPN Network is a   |
|                      | Tenant Net Internal  | “simple” collection  |
|                      |                      | of Devices. It       |
|                      |                      | defines the          |
|                      |                      | connection between   |
|                      |                      | the Devices and the  |
|                      |                      | Connectivity Suite.  |
|                      |                      | This can either be   |
|                      |                      | directly or through  |
|                      |                      | a VPN tunnel.        |
+----------------------+----------------------+----------------------+
| VPN Network Address  | Tenant-internal      | The address block of |
| Block                | Network              | a specific VPN       |
|                      |                      | network.             |
+----------------------+----------------------+----------------------+
| VPN Network Address  | Tenant Network in    | The address block of |
| Block in Platform    | Home Network         | a specific VPN       |
| Network              |                      | network inside the   |
|                      |                      | Platform Address     |
|                      |                      | Block.               |
+----------------------+----------------------+----------------------+
| VPN subnet Pool      | Tenant Pool          | Collection of        |
|                      |                      | subnets of the same  |
|                      |                      | size within the      |
|                      |                      | Platform address     |
|                      |                      | block.               |
+----------------------+----------------------+----------------------+

.. _legal_aspects:

Legal Aspects
=====================================


**Legal Considerations**

This document contains proprietary information of NetModule AG. No part of the work described herein may be
reproduced. Reverse engineering of the hardware or software is prohibited and is protected by patent law.
This material or any portion of it may not be copied in any form or by any means, stored in a retrieval system,
adopted or transmitted in any form or by any means (electronic, mechanical, photographic, graphic, optic or
otherwise), or translated in any language or computer language without the prior written permission of
NetModule AG.

The information in this document is subject to change without notice. NetModule AG makes no representation or
warranties with respect to the contents herein and shall not be responsible for any loss or damage caused to the
user by the direct or indirect use of this information. This document may contain information about third party
products or processes. This third-party information is out of influence of NetModule AG therefore NetModule AG
shall not be responsible for the correctness or legitimacy of this information. If you find any problems in the
documentation, please report them in writing by email to info@netmodule.com at NetModule AG.
While due care has been taken to deliver accurate documentation, NetModule AG does not warrant that this
document is error-free.

NetModule AG is a trademark and the NetModule logo is a service mark of NetModule AG.

All other products or company names mentioned herein are used for identification purposes only and may be
trademarks or registered trademarks of their respective owners.

The following description of software, hardware or process of NetModule AG or other third-party provider may be
included with your product and will be subject to the software, hardware or other license agreement.