Dashboard
*********************************

The Dashboard is supposed to give a quick status overview of the Connectivity Suite and its devices.

.. figure:: images/dashboard.png
  :name: sample-dashboard
  :width: 700px

  Dashboard
..

Tiles
-------------------------------------

There are multiple tiles providing different information.

Devices in Networks tile
===================================

This tile shows all not hidden devices assigned to a network (i.e. no longer in provisioning) grouped by their current status.

The following status-grouping is applied:

* Offline (<5m): offline less than 5 minutes
* Offline (<1h): offline less than 1 hour
* Offline (<24h): offline less than 24 hours
* Offline (>24h): offline more than 24 hours
* Online
* Unknown

By clicking on the corresponding status in the status list, the user will be directed to the Devices page and the corresponding filter will be applied.

Devices in Provisioning tile
===================================

This tile shows all not hidden devices in provisioning (i.e. not yet assigned to a network) grouped by their current status.

The same status-grouping is applied as for the :numref:`Devices in Networks tile` :ref:`Devices in Networks tile` tile.

By clicking on the corresponding status in the status list, the user will be directed to the Devices page and the corresponding filter will be applied.

Device Models tile
===================================

This tile shows all not hidden devices grouped by their device model.

By clicking on the corresponding device model in the device model list, the user will be directed to the Devices page and the corresponding device model filter will be applied.

VPN Networks tile
===================================

This tile shows all VPN networks including the Backend and the Provisioning networks grouped by their status (online, offline, unknown).

By clicking on the corresponding status in the status list, the user will be directed to the Networks page and the corresponding filter will be applied.

Jobs tile
===================================

This tile shows the most recent jobs grouped by their status.

The date range to cover can be changed in the tile. The following date ranges are available:

* Last hour
* Today
* Last 24 hours
* Last 3 days
* Last 7 days (default)
* Last 30 days 

By clicking on the corresponding status in the status list, the user will be directed to the Jobs page and the corresponding filter will be applied.

Software tile
===================================

This is a series of tiles, one per device type (e.g. Router, Switch, etc.). They show the software versions currently used. It covers only devices providing this information. Only not hidden devices are incorporated.

By clicking on the corresponding software version in the software version list, the user will be directed to the Devices page and the corresponding filter will be applied.

Notifications & Warnings tile
===================================

This tile provides notifications and warnings about the platform state. They get provided once a day to the user upon login, but can be triggered manually by clicking the "Refresh now" button anytime. These messages should make the user aware about situations which might impact the performance or correct functioning of the platform.
