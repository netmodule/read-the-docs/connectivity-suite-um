.. _child:

Child Device Access
*********************************

Overview
====================================

Accessing Child Devices by utilizing Connectiviy Suite can be done in two different ways. 

- Network Address Translation (NAT) on Parent Device
- Custom Routes on Parent Device

Given by the network architecture, Connectivity Suite, the Parent Device and the Child Device reside in different IP Networks. In order to access (end)devices, located in Local Area Networks behind routers (as per :numref:`network_setup`), either a NAT Table or Custom Routes are required to be setup. (see :numref:`network_setup`).

.. figure:: images/networksetup-general.png
  :align: center
  :width: 350px
  :name: network_setup

  Network Setup Devices and Connectivity Suite


Network Address Translation (NAT)
==========================================
To access Child Devices, such as CCTV Cameras, WiFi Access Points, etc., the Device VPN Network address needs a Network Address Translation (NAT) to the Local Area Network address.
For example, the Device VPN Network 10.0.0.0/24 is translated into the Local Area Network 172.16.0.0/24, as per the illustration in :numref:`network_network_nat`.

.. Note:: The Subnetmasks of both, VPN Network and the Local Area Network, have to be identical in order for the NAT to work.


.. figure:: images/natnetworksetupdetailedlong.png
  :width: 300px
  :name: network_network_nat

  Network Setup in Network Network

..

NAT Example Use Case
-----------------------------

Network Architecture:

- VPN Network using an IP Address Block of /19
- Device VPN Netowrks using IP Subnetmasks of /24
- Local Area Netowrks using the same IP Subnetmask size of /24
- The Child Device is given the IP address of 172.16.0.111
- A Network NAT from LAN (17.0.0.0/24) to the Device VPN Network (10.0.0.0/24) is setup

Benchmark Data / Limitations of NAT Example Use Case:

- Max. 32 routers can be connected to a VPN Network
- Max. 256 Child Devices that may be connected to a router

Router Settings for NAT
-----------------------------------

In order to carry out a NAT, various steps are necessary which are explained in this chapter  :numref:`Child Device Remote Access`


Custom Routes
==========================================
For the same purpose of setting up direct access to Child Devices, Custom IP Routes may be setup as an alternative to NAT or on top of it.
The working principle of Custom Routes is depicted in the illustration :numref:`Custom_Routes_Overview` below.
In contrast to a NAT based Network Design, direct Routes require the LAN Addressing to be unique within a VPN Network. Custom Routes are configured on top of other Routes, such as those required by the System of Connectivity Suite. Furthermore Custom Routes can be configured on a "as needed" basis and do not require the entire system to be configured with them. 

.. figure:: images/Custom-Routes-Netwk-Overview.png
  :width: 400px
  :name: Custom_Routes_Overview

  Custom Routes Principle

Configuration
------------------------------------------

Enabling custom routes, requires the feature to be switched on under the general settings.
In a second step, the effective custom routes are to be configured per device under the device details tab.

.. figure:: images/Enable_Custom_Routes.png
  :width: 600px
  :name: Enable Custom Routes

  Enabling of Custom Routes


