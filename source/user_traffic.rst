
User Traffic
*********************************

By enabling and configuring the User Traffic feature, the Connectivity Suite acts as a router between the (end) devices and the customer's backend systems.
This allows for instance to connect a ticket vending machine to an accounting application via the VPN infrastructure operated by the Connectivity Suite. This is achieved by additional rules being pushed to the routers, so that in addition to the device managment traffic, also specific User Traffic is routed over the same VPN tunnel/infrastructure.

User Traffic must initially be enabled in the Command Line Tool; further settings for routing and masquerading can then be configured through the CS UI/API.


Enabling User Traffic
=====================

The User Traffic feature must first be enabled in the Command Line Tool (cs-cmd), by selecting "Configure User Traffic Feature".

By doing so, the firewall on the CS server is set to allow incoming traffic destined to the Platform Network.

Apart from the configuration in Connectivity Suite, corresponding routes in the Backendsystems are required, pointing to the CS Platform Network.



Traffic from the company network to routers and end devices
===========================================================

By following the steps above, the routers are accessible from the company network.

Accessing end devices behind the routers, NAT rules on the routers are required (see :numref:`NAT Rules` below).

Routers and end devices are reachable via their platform address.


Traffic from routers and end devices to company network
===========================================================

Which traffic to be sent trhough the VPN infrastrucutre, origing from routers and end devices & destined to the company network, is configured as follows:

- **Routing**: which traffic to be routed through CS to company network
- **Masquerading**: how the source IP address of user-traffic is formed

Reminder: For end devices to access the company network, NAT rules on the routers are required (see :numref:`NAT Rules` below).

Routing
---------------

The logic which traffic is to be sent through the CS VPN infrastructure is managed by routes on the routers itself. These routes are pushed by CS, according to the setting below. 


+-----------------+-----------------------------------------------------------------------------------------------------+
| **None**        | No routes are pushed to the router. The device's traffic is routed to its existing default gateway. |
+-----------------+-----------------------------------------------------------------------------------------------------+
| **None Except** | Only traffic destined for specific networks is routed through the CS.                               |
|                 | All other traffic is routed to the device's existing default gateway.                               |
+-----------------+-----------------------------------------------------------------------------------------------------+
| **All**         | All traffic is routed through the CS, i.e. the CS acts as the device's default gateway.             |
|                 | **IMPORTANT:** If routing  is set to "All", you won't be able to access the router                  |
|                 | via its WAN address anymore! Access will only be possible                                           |
|                 | via the LAN address or the Connectivity Suite (either via "Open Web Interface" button               |
|                 | or Service Access connection)!                                                                      |
+-----------------+-----------------------------------------------------------------------------------------------------+

The routing options can be managed on platform level as well as on individual VPN Networks.


Masquerading
--------------------

Traffic originating from routers and end devices, destined to a backend network may have one of the following source IP addresses:

- device's platform address, if the source address is **not** masqueraded
- CS server IP address, if source address **is** masqueraded

Masquerading options:

+------------------+-----------------------------------------------------------------------------------------------------+
| **None**         | No masquerading is done.                                                                            |
+------------------+-----------------------------------------------------------------------------------------------------+
| **None Except**  | Traffic destined for specific networks is masqueraded. All other traffic is not masqueraded.        |
+------------------+-----------------------------------------------------------------------------------------------------+
| **All**          | All traffic is masqueraded.                                                                         |
+------------------+-----------------------------------------------------------------------------------------------------+
| **All Except**   | Traffic destined for specific networks is not masqueraded. All other traffic is masqueraded.        |
+------------------+-----------------------------------------------------------------------------------------------------+

The masquerading options can only be managed on platform level.


NAT Rules
=========

For the User Traffic to work, NAT rules must be configured on the routers.

**Incoming direction - Backend to end devices** 
- "VPN IP address block" to "LAN IP address block" NAT rule

**Outgoing direction - End devices to Backend**
- "LAN IP address block" to "VPN IP address block" NAT rule

.. note::  These NAT rules are not part of the User Traffic Settings; they have to be configured separately. See :numref:`Network Address Translation (NAT)` for more information on NAT.

Disabling User Traffic
======================

Disabling the User Traffic feature in the Command Line Tool (cs-cmd) has the following consequence

- The firewall on the CS server no longer accepts incoming traffic that is directed to the Platform Network.
- No routes are pushed to the routers anymore.
- All masquerading rules are removed.
