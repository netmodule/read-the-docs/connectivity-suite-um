Variables & Bulk Editor
******************************************************


The main advantage of Variables is to reduce time required for deployments, especially for many devices to be configured at the same time, with each device having individual parameters being unique.

System & Custom Variables
=====================================================
Variables consist of a parameter key & value pair, which is available system wide, to be used in device template configs and snippets.


.. figure:: images/Workflow-Variables.png
  :name: Workflow-Variables
  :width: 700px

  Workflow Variables
..

The administration of Variables is done on a global level under systems settings. 
In addition to Variables provided by the system, custom ones may be created as desired.

Custom Variables
-------------------------------------
Follow the steps below, to create custom variables

1. Create a new table entry 
2. Enter Custom Variable
3. Save entry


.. figure:: images/variables_global_settings.png
  :name: Variables-Global-Settings
  :width: 700px

  Variables Global Settings
..

For custom Variables, a default value must be set. In addition, either the network or device specific value, can be set. This is done on the corresponding detail page under the tab “Variables”, by clicking the save icon once entered.

.. note:: Once a variable is created, its name cannot be changed afterwards.

Default value and description of a variable can be modified once created. When doing so, the new value will be propagated down to Network and Device level, where no custom value has been configured before (See :numref:`variables-value-propagation`). With the consequence that all values which have been set individually (override) on either network and/or device level, do not change when the default value for a variable is being modified. 
Both, system provided and custom created Variables, are distributed to Networks and Devices and therefore available for deployments. 

.. figure:: images/value_propagation.png
  :name: variables-value-propagation
  :width: 700px

  Variables Values Propagation
..

Bulk Import
=====================================================
In addition to managing the values of Variables through the WEB interface, they can be edited in an .xlsx document and be imported in one batch. 
It is recommended to export a device list first (see :numref:`Device List Export`) and start editing parameters there before importing it.
The greyed-out fields in the .xlsx sheet previously exported, must not be edited as these are non-configurable items. Either the column “DeviceID” or “MacAddress” is required for the device identification when importing the .xlsx sheet.
Fields that are kept blank cause no action when importing.


Device List Export
=====================================================
Device lists with custom parameters can easily be generated using the Export function.
In a first step, select the devices, followed by “Export Device List” under Actions, which opens a Modal Dialog. 
Chose which fields to be included in the .xlsx formatted list. By selecting “Standard” the commonly used parameters are included. This list may be edited, by adding device specific values for the given variables and for a later import to Connectivity Suite.



Deployments using Variables
=====================================================

Variables can be used system wide for different deployments, such as Configuration, Snippets, etc.
Using an example of a configuration deployment, the following steps are required.
Under the menu item Artifacts, select Configuration.
1.	Add a new Configuration, which serves as a template for mass deployments
2.	Once created navigate to the Editor tab of the Configuration
3.	Replace Configuration values which should be device dependent (see :numref:`Config-Editor-Variables`) by inserting a Variable.
4.	Choose a variable in the drop-down list
a.	Repeat for additional Variables to be set

In the example below, the system hostname uses a variable which means the corresponding value will be inserted which is set for the device.

.. note:: Make sure the config is saved before continuing to the next step

.. figure:: images/Config-Editor-Variables.png
  :name: Config-Editor-Variables
  :width: 700px

  Variables Configuration Editor
..

For the deployment of the configuration, follow the instructions documented under :numref:`Deploy a Configuration`

