Certificate Management
******************************************************

The built in Certificate Authority handles the Lifetime by issuing new Certificates as well as deploying them by replacing the old ones. This includes certificates of VPN Networks, Service Access, Devices and HTTPs for WEB access.
This happens automatically in the background with no interaction needed.

Certificate Lifetimes
=====================================================
By default, when Connectivity Suite was installed with version 3.3.x or newer, the following certificate Lifetimes are in use:


+------------------+--------------+---------------------------+
| **Certificate**  | **Lifetime** | **Renewal before expiry** |
+------------------+--------------+---------------------------+
| **Root**         |    1095d     |                  365d     |
+------------------+--------------+---------------------------+
| **HTTPS**        |      90d     |                   30d     |
+------------------+--------------+---------------------------+
| **Network**      |     730d     |                  243d     |
+------------------+--------------+---------------------------+
| **Device**       |      90d     |                   30d     |
+------------------+--------------+---------------------------+
| **Service Acc.** |      90d     |                   N/A     |
+------------------+--------------+---------------------------+


Updating or migrating Connectivity Suite from a previous release, Certificate Lifetimes are carried over as follows. 


+---------------------+---------------+--------------+
| **From**            | **To**        | **Lifetime** |
+---------------------+---------------+--------------+
| **v2.x**            | v3.3 or above | 10 years     |
+---------------------+---------------+--------------+
| **v3.2.x** or below | v3.3 or above | 10 years     |
+---------------------+---------------+--------------+


Certificate Renewal
=====================================================

Using the standard settings for Certificate Management (Custom Certificate Settings switched off), Certificates are automatically renewed and deployed before they expire. 
The renewal process may happen at any time, with the condition that there is an active connection to the corresponding device / network, etc. 
 

.. note:: Replacing the certificate, may cause a short connection interrupt.


Individual Settings
=====================================================

If desired, both the Certificate Lifetime as well as the period for renewal may be configured using individual values. The deployment window of the newly issued Certificates can also be specified if needed.


.. warning:: Modifying Certificate Lifetime values can cause disconnects and potential lockouts of devices no longer being able to re-connect to Connectivity Suite.

Before individual settings may be used, the auto function needs to be switched off. This is done, under Global Settings.


1. Enable Custom Settings for Certificate Management
2. Save before proceeding to the Certificates Tab


.. figure:: images/cert-mgt/cert-global-on.png
  :name: Certificate Custom Settings
  :width: 500px

  Certificate Custom Settings
..

Custom Lifetime & Renewal Settings
--------------------------------------------

With exception to the Root Certificate, which is on global level, there are two options to which level the settings apply to.

* Platform wide:          Lifetime and renewal applies system wide
* Individual per Network: Settings can be modified on each Network individually


When changing the lifetimes, it is required that certificates on lower levels have a shorter lifetime than their parent certificates.
The renewal period is derived by 1/3 of the certificate Lifetime but may be adjusted if desired.

On network level, the same Lifetime and renewal parameters can be found and adjusted, if “Individual per network” is set. For that, see the Certificate Settings Tab on each Network detail page.


.. note:: Depending on the changes to the Certificate Lifetime being made, there can be consequences, which need to be addressed beforehand, in order to not lose the connection to devices. Especially for pre-provisioned devices that have not yet established an active connection to Connectivity Suite may require a new Provisioning Configuration which is to be manually uploaded.

The following examples show the general behavior when changing the Certificate Lifetimes.

**Impact When Shortening Lifetime**

.. figure:: images/cert-mgt/lifetime-shorter-t-current.png
  :name: Certificate Shorter than Current
  :width: 600px

  Certificate Lifetime Shorter than Current
..

Changing the Certificate lifetime to be shorter than the current lifetime: As soon as the new Certificates have been deployed, the pre-existing ones will be revoked. 

.. warning:: Provisioning and Service Access Certificates are revoked immediately, when a new lifetime is set, which is shorter than the existing one. Therefore, new provisioning-configs need to be issued and loaded into devices manually. The same for service access, for which the new Certificate needs to be distributed.


**Impact When Increasing Lifetime**

.. figure:: images/cert-mgt/lifetime-longer-t-current.png
  :name: Certificate Longer than Current
  :width: 654px

  Certificate Lifetime Longer than Current
..

When the Lifetime is increased, the current certificates remain unchanged active and valid as per their settings.


Custom Certificate Deployment Schedule
-----------------------------------------------

Newly generated Certificates need to be deployed before the current one expires. The renewal process may happen at any time when the certain entity (network, devices, etc.) is online. This behavior can be modified, and a specific schedule configured by when the deployment of newly issued certificates should be deployed. Given the target entity has an active connection to the system.
When the effective deployment of these certificates should happen may be specified in the same manner as for config deployments see :numref:`Deploy a Configuration` 

