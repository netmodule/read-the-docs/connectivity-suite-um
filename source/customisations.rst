Customizations
*****************************************

There are several ways to customize Connectivity Suite to meet specific needs. The following chapter contains some options and examples of how to realize things that provide an additional value for certain applications.

.. warning:: While many of those options are being used in the field, they can break the system or reduce its performance. Please make sure to test your changes in a staging environment before deploying them to production.
  
  NetModule does not provide support for customisations.


Additional Docker Containers
----------------------------

Additional containers can operated in Connectivity Suite by adding them to the ``cs/app/compose.override.yml`` file. This file is not part of the standard installation, but can be added manually.


NTP Server
==========
The following example adds an NTP server to the Connectivity Suite. The NTP server is a simple container running the *chrony* NTP server. The container is added to the Connectivity Suite by adding the following lines to the ``compose.override.yml`` file:

.. code-block:: yaml

  services:
    ntp:
      image: dockurr/chrony
      container_name: ntp
      ports:
        - 123:123/udp
      restart: always
      networks:
        - cs-net


Grafana
=====================

With the following example Grafana server may be added to Connectivity Suite.

The Grafana UI is exposed via the Connecitivty Suite Web Proxy Service called *Traefik* under ``/grafana``. This is done with the help of labels. More about Traefiks use of labels can be found here: https://doc.traefik.io/traefik/v3.2/providers/docker/.

The container is added to the Connectivity Suite by adding the following lines to the `compose.override.yml` file:

.. code-block:: yaml

  services:
    grafana:
      image: grafana/grafana:latest
      container_name: grafana01
      restart: always
      environment:
        - GF_SERVER_ROOT_URL=/grafana
        - GF_SERVER_SERVE_FROM_SUB_PATH=true
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.grafana-router.entryPoints=websecure"
        - "traefik.http.routers.grafana-router.rule=PathPrefix(`/grafana/`)"
      networks:
        - cs-net

.. warning:: This is a simplified example. The Grafana container should be configured with a persistent storage for the data and the configuration. The example does not include a persistent storage.

Start or update instance with extra services
============================================

After changes to the compose.override.yml file, the Connectivity Suite needs to be restarted. This can be done by running the following command in the ``cs/app`` directory:

``docker-compose up -d``

Customising the Connectivity Suite API / Proxy settings
-------------------------------------------------------

The Connectivity Suite *API* and the *Proxy* settings can be customised by changing the following file ``cs/app/app-data/api/custom.json``. The following example shows how to add a override the certificate renewal schedule (default 2am UTC) to 10pm UTC:

.. code-block:: json

  {
    "App": {
      "Certificate": {
        "DailyCertificateRenewalAndCleanupHour": 22
      }
    }
  }

Triggering scripts on VPN servers
---------------------------------

.. warning:: You can easily break the VPN Servers or make them more vulnerable by running custom scripts. Please make sure to test your scripts in a staging environment before deploying them to production.

In rare cases a user might want to trigger some custom scripts running on the VPN servers. This can be done via the API by using the following endpoint:

``PUT /api/v3/Devices/{id}/staticScripts``

.. note:: The ``{id}`` above is **not the NetworkId.** To find the Id of the VPN Server Device, you can have a look at the ``vpnServerId`` property in the response of the ``GET /api/v3/Networks`` endpoint.

The following 4 script hooks are available:

* **preStartup**

  This is run before the offical startup script is run.

* **postStartup**

  This is run after the offical startup script is run.

* **preReconfigure**

  This is run before the server reconfigure script is run which happens with every change on the network (e.g. new Device is added etc.)

* **postReconfigure**

  This is run after the server reconfigure script is run which happens with every change on the network (e.g. new Device is added etc.)

You can retrieve the current scripts by using the following endpoint:

``GET /api/v3/Devices/{id}/staticScripts``

If you want to see the logs of the scripts, you can use the following endpoints:

``GET /api/v3/Devices/{id}/logs/readable`` or ``GET /api/v3/Devices/{id}/logs`` to get a zip file with all logs.
