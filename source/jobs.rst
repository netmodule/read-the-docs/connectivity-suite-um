.. _jobs_overview:

Jobs
*****************************************

An overview table featuring all Jobs is available on the *Jobs* page, facilitating the management of pending and failed Jobs.
In the list part of the page, you can find all jobs, complete with their status and overall progress.
Upon selecting a specific job from the list, the corresponding job details are presented in the detail part of the page (see :numref:`Jobs Overview`).


.. figure:: images/jobs_list.png
  :width: 1000px
  :align: center
  :name: Jobs Overview

  Jobs overview
..

Job Details
-------------------------

In the *Detail* part of the page the Job Details are displayed:

+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Job**       | Shows the name and the type of the Job.                                                                                                                                                                                                                   |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **State**     | Shows the state of a Job, the progress of its tasks and the Schedule. The different states are described in :numref:`job_states` and the Schedule Types in :numref:`job_schedules`.                                                                       |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Job Tasks** | Shows each Task, often associated with a specific device, the Tasks State, when it was last triggered, the current Step (if applicable) and the progress of its steps (if applicable). The different states are described in :numref:`job_task_states`.   |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Steps**     | Certain tasks consist of multiple steps. Those steps indicate where a task might got stuck and how to manually fix an issue with the device.                                                                                                              |
|               | Steps have a Status and they have *Started At*, *Skipped At* and *Completed At* dates indicating their progress.                                                                                                                                          |
+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


.. figure:: images/job_details.png
  :width: 1000px
  :align: center

  Job Details
..

.. _job_schedules:

Job schedules
-------------------------

Fire and forget
=========================

The Job will be executed immediately after it was created. If it fails, it will retry every 5 minutes until the Expires At date is reached. Fire and forget Jobs expire after 30min.

Scheduled
=========================

The Job will be executed during the specified *date range* under the consideration of the specified *time window* and *weekdays*. If the Job fails, it will retry every 5minutes during those windows until the end of the specified date range (Expires at).

.. figure:: images/job_schedule.png
  :width: 200px
  :align: center
  :name: Jobs Schedule

  Jobs Schedule
..

.. _job_states:

Job States
-------------------------

Indicates in which state a **Job** is, between creation and completion.

+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Job Status          | Description                                                                                                                                                                                                                                                                                               |
+=====================+===========================================================================================================================================================================================================================================================================================================+
| Scheduled           | Pending for execution. Typically due to the start date has not been reached yet or the execution time window is currently closed (the current time is not between start and end time and/or the date of the week does not allow the execution).                                                           |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Running             | Job is being executed.                                                                                                                                                                                                                                                                                    |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paused              | The paused state typically occurs when the schedule window has closed.                                                                                                                                                                                                                                    |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Succeeded           | Successfully completed with all tasks processed as expected.                                                                                                                                                                                                                                              |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Failed              | Due to an error or failure the job was not completed.                                                                                                                                                                                                                                                     |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Partially Succeeded | Not all of the tasks belonging to a job were completed successfully processed.                                                                                                                                                                                                                            |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Cancelled Manually  | Manually cancelled before completion.                                                                                                                                                                                                                                                                     |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Replaced            | A different Job has replaced the current Job.                                                                                                                                                                                                                                                             |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Unknown             | The state of the Job is not known or has not been set.                                                                                                                                                                                                                                                    |
+---------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+



.. _job_task_states:

Task States
-------------------------

Represents the various states a **Job Task** can have during its lifecycle.


+-------------+---------------------------------------------------------------------------------------+
| Status      | Description                                                                           |
+=============+=======================================================================================+
| Scheduled   | Scheduled to run at a specified time.                                                 |
+-------------+---------------------------------------------------------------------------------------+
| Running     | The Job Task is currently in progress.                                                |
+-------------+---------------------------------------------------------------------------------------+
| Rescheduled | Rescheduled to run at a different time.                                               |
+-------------+---------------------------------------------------------------------------------------+
| Succeeded   | Completed successfully.                                                               |
+-------------+---------------------------------------------------------------------------------------+
| Failed      | An error was encountered during execution, therefore it not complete successfully.    |
+-------------+---------------------------------------------------------------------------------------+
| Cancelled   | It was cancelled before it could complete.                                            |
+-------------+---------------------------------------------------------------------------------------+




.. _system_jobs:

System Jobs
-------------------------

The system generates various Jobs that are automatically excluded from the Jobs List by default. These jobs are created to guarantee the proper functioning of the Connectivity Suite, such as updating Device Certificates or retrieving the most recent Device Information.