Remote Access
*********************************


Connectivity Suite offers two options for remote access to Devices (incl. end/child-devices).

* **Web Proxy Access via Browser**
  The Web Interface of Devices can be reached through a Web Proxy. In addition there you can enable Proxy Records :numref:`Proxy Records Option (advanced)`, which allow a static and direct web access to devices as well as to end-devices.

* **Service Access via VPN**
  As per the chapter :numref:`Device access via OpenVPN client`, a full network service access allows managing devices and end-devices.


Device access via Web Proxy
============================

The Connectivity Suite web proxy enables access to the web interface of the Child Devices to the Connectivity Suite OpenVPN servers without having to run an OpenVPN client locally.

Default Option (basic)
-------------------------------
Accessing the web interface of the devices click on the "Open Web Interface" button in the Device Details (see :numref:`Open web interface via web proxy`).

.. figure:: images/openwebinterface_webproxy.png
  :name: Open web interface via web proxy

  Open web interface via web proxy
..

.. note:: Make sure that you have added a **devices** CNAME DNS record. See (see :numref:`cname`) for more details.

Proxy Records Option (advanced)
-----------------------------------
Besides the basic option of accessing devices and end-devices one at a time, the feature **Proxy Records** can be configured. This offers two benefits:

* Multiple sessions open at once by using the following url pattern ``xxx-xxx-xxx-xxx.devices.mycs.com``, e.g. ``10-240-3-45.devices.mycs.com``.
* Proxy Records mapping an *Alias* to a specific *IP* and *Port* of any Device, such as a CCTV camera, e.g. ``camera-1-bus-23.devices.mycs.com``.

.. figure:: images/proxy-records/ui-proxy-graphic.png
  :width: 400px
  :name: Proxy Records Principle

  Proxy Records Principle
..

Enable Proxy Records
---------------------------

You must use the ``cs-cmd`` tool to enable or disable the Proxy Records feature. This can be done through the command line interface by navigating to the ``> Configure Proxy Records Feature`` menu and setting the status to ``> enable``.

Depending on which HTTPS CA is being used, the corresponding configurations need to be made. 

* **LetsEncrypt**
   * Add CNAME according to instructions provided in cs-cmd.
   * Configure the DNS challenge
   * Set the Provider Code and Variables, corresponding to  `Traefik Let's Encrypt Documentation <https://doc.traefik.io/traefik/v3.2/https/acme/#providers>`_ ``> set provider / > edit provider``
   * Environment Variables ``> add variable / > edit variable / > delete variable``
   * Save settings with ``> save and exit``

* **Own HTTPS Certificates**
   * Add CNAME according to instructions
   * Generate new certificates including the given wildcard SAN
   * Replace the certificates

* **Generated Certificates**
   * Add CNAME according to instructions

Configure Proxy Records
---------------------------

Proxy Records are managed under Global Settings. For each Device or End-Device that needs to be reached through a custom DNS record, a Proxy Record is required, as illustrated in the image below. Each record covers only one TCP Port, so two or more records are required if multiple targets on a device using different ports need to be reached.

.. figure:: images/proxy-records/ui-proxy-settings.png
  :name: Proxy Records List

  Proxy Records Configuration

Table Controls

#. Create a new Proxy Record
#. Save entry / entries
#. Update table

.. note:: Changes may take up to 5 Minutes to take effect.

Explanation & meaning of table entries:

+----------------+-------------------------------------------------------------------------------+-------------------+
| Column         | Explanation                                                                   | Example           |
+----------------+-------------------------------------------------------------------------------+-------------------+
| Alias          | Alias name, used for accessing the corresponding device via DNS through HTTPS | camera7-tram3-nw1 |
+----------------+-------------------------------------------------------------------------------+-------------------+
| Target Address | Specify the target IP of the corresponding device or end-device               | 10.236.1.38       |
+----------------+-------------------------------------------------------------------------------+-------------------+
| Target Port    | Specify the target port of the corresponding device or end-device             | 443               |
+----------------+-------------------------------------------------------------------------------+-------------------+
| Network        | Corresponding VPN Network, owning the matched IP Address Block                | Network1          |
+----------------+-------------------------------------------------------------------------------+-------------------+
| Device         | Corresponding Device, owning the matched IP Address Block                     | tram3             |
+----------------+-------------------------------------------------------------------------------+-------------------+

#. Proxy Records are not updated automatically, if a router gets a different address (e.g., by moving to another network), the corresponding proxy records will no longer work or must be updated manually (Target Address).
#. For proxy records that point to a connected device, we recommend configuring the router to assign a fixed IP to the connected device. Otherwise, the proxy record can also become invalid suddenly if the connected device receives a different IP.



Device access via OpenVPN client
===================================

To access the Devices, Connectivity Suite provides OpenVPN to access.
A VPN connection to the Provisioning, the Backend or any custom VPN Network Server must be established.
This means the user needs to run an OpenVPN client.
The configuration and certificates needed for the VPN connection can be downloaded from the Connectivity Suite via the Service Access function.

Installing OpenVPN Client
--------------------------
The OpenVPN client to access the devices can be downloaded from https://openvpn.net/community-downloads/.

.. warning:: While the VPN Connect client often works, make sure you are using the **Community Edition** of the OpenVPN client. The commercial version of the OpenVPN client is not officially supported by the Connectivity Suite.

OpenVPN access
----------------------

There are three types of Service Access:

1. VPN Network: The user can access any Device within the VPN Network by using the VPN Address of the Device.This is helpfull if the user shall access only Devices of a specific VPN Network.
2. Platform Network: The user can access any Device within the Platform Network by using the Platform Address of the Device. This is helpfull if the user shall access Devices of different VPN Networks.
3. Provisioning Network: The user can access any Device within the Provisioning Network by using the Provisioning Address of the Device.



4. Navigate to the page “Network” of the Connectivity Suite. Select the Network where the required Device is assigned.
5. Click on “Download OpenVPN client configuration” at the Detail dialogue box to download the OpenVPN client configuration.

.. figure:: images/download_vpn.png
  :name: Download VPN config

  Download VPN config
..

3. Start the OpenVPN client on your client pc and upload the downloaded file from step 1 into the OpenVPN client to establish a connection with the Connectivity Suite.
4. After the connection has been established Navigate to the page “Devices” and select the required Device in the main dialogue table.
5. Use the IP-address shown in the Detail dialogue box to access the Device.

.. figure:: images/openwebinterface_webproxy.png
  :width: 900px
  :name: Open web interface

  Open web interface
..

Child Device Remote Access
==========================

 To access Child Devices the router must perform a network NAT.
 In order to carry out a NAT, following router settings are necessary:

 1.	Open the web interface of the router, open the Firewall/Inbound Rules page and create a new rule
 2.	Select "network" for the mapping to execute a network NAT.
 3.	Select the interface. The interface must be the OpenVPN tunnel which is used to connect to the Connectivity Suite.
 4.	Add the Device VPN Network Block Address. If its unclear what the Device VPN Network Block Address ist check chapter :numref:`child`.
 5.	Add the LAN as Redirect address/netmask and apply the settings.

 .. figure:: images/natsettingsrouter.png
   :width: 700px
   :name: nat_settings_router

   NAT Settings NetModule Router

 ..

 6.	Select now the router in the Device Liste and open the tab Child Device in the Detail dialogue box and click on the "Scan again" button. The Child Devices are now displayed in the table.

 .. figure:: images/natscandevices.png
   :name: Scan Devices Image
   :width: 700px

   End Devices View Connectivity Suite

 ..

Automatic adaption of NAT rules
===============================

.. note:: Only NAT rules whose settings for incoming/outgoing interfaces match the OpenVPN tunnel used by the Connectivity Suite (usually TUN1) are adapted. This prevents unwanted changes to uninvolved rules.

In order to access connected devices behind a router via the Connectivity Suite, NAT rules must be defined on the router which map VPN IP addresses to LAN IP addresses.

When a router is moved to another VPN Network, these NAT rules have to be adapted, as the VPN IP addresses will change. These adaptions are made automatically and are included in the new device configuration which is deployed during the device move process.

Both the rules for host mapping and for network mapping are adapted, in each case both inbound and outbound rules.

Use in Provisioning configurations
----------------------------------

For calculating the adaption, only the host part of the VPN IP address is relevant. This allows the user to define generic NAT rules in the Provisioning configuration where the network part of the VPN IP address is just a placeholder. This generic rules will then be adapted to individual rules as soon as the device is moved to a network.

**Example for host mapping**

Let’s assume you are dealing with /25 device subnets (128 addresses), and the connected device with the LAN IP address 192.168.1.220 should be accessed via the second usable VPN IP address of the device subnet (the first usable address is the router’s own VPN IP address).
The VPN IP address we use for this rule is then 10.0.0.2, where 10.0.0 is the placeholder for the network part and .2 is the relevant host part.

The corresponding settings in the Web Manager would then be:

.. figure:: images/napt_host_provisioning.png
  :name: Host NAPT settings for Provisioning Configuration

  Host NAPT settings for Provisioning Configuration
..

Above rule says that the second usable VPN IP address of the device subnet is mapped to 192.168.1.220.

Let’s further assume we are moving a device with the above configuration to a network with /25 device subnets, and that the first 3 device subnets in this network are already used. As the next free device subnet is 10.0.1.128/25, the above rule would then be automatically adapted as follows:

.. figure:: images/napt_host_after_move.png
  :name: Host NAPT settings after device was moved to a VPN Network

  Host NAPT settings after device was moved to a network
..

**Example for network mapping**

For rules which map a complete LAN subnet, the whole VPN IP address is just a placeholder.

Let’s assume we want to access the whole 192.168.1.0/24 LAN of a device via VPN IP addresses. For the VPN IP address, we use 10.0.0.0 as a placeholder. The corresponding settings in the Web Manager would then be:

.. figure:: images/napt_network_provisioning.png
  :name: Network NAPT settings for Provisioning Configuration

  Network NAPT settings for Provisioning Configuration
..

Let’s further assume we are moving a device with the above configuration to a network with /24 device subnets, and that the first 14 device subnets in this network are already used. As the next free device subnet is 10.0.14.0/24, the above rule would then be automatically adapted as follows:

.. figure:: images/napt_network_after_move.png
  :name: Network NAPT settings after device was moved to a VPN Network

  Network NAPT settings after device was moved to a network
..

Moving the device from a network to another network
---------------------------------------------------

The automatic adaptions described above are not restricted to the initial move of a device from provisioning to a network. They are also carried out if the device is later moved to other networks.

