User Management
***************************

The Connectivity Suite user hierarchy consists of three different user roles.

Platform Admin
====================

The Platform Admin is the highest in the role hierarchy. A Platform Admin can administer the complete Connectivity Suite instance. This means he can administer VPN Networks, Devices and users.

Network Admin
=================

A Network Admin can only administer Devices connected to a specific VPN Network. A Network Admin can assign the Network Admin and Network user roles for VPN he administrates to other users.

Network user
================

The Network user only can access and monitor Devices within specific VPN Network. However, a Network user is not allowed to manipulate Devices via the Connectivity Suite.

User rights table
==================

.. figure:: images/userrightstable.png
  :align: center
  :name: User rights table image

  User rights table
..

Assigning user rights
=======================

1. Navigate to the page “User Management” of the Connectivity Suite UI by clicking on the tool icon in the Support bar.

.. figure:: images/usersmanage.png
  :align: center
  :width: 600px
  :name: Manage users

  Manage users
..

2. Click on "Actions" at the upper right corner of the Main dialogue box and click "Add user" to add a user.

.. figure:: images/add_user.png
  :align: center
  :width: 600px
  :name: Added user

  Added user
..

3. Fill out the required fields and click on “Add user” to add the user.
4. The added user must now be shown in the Main dialogue box in the table.

When creating the user following parameters can be set

+--------------------+------------------------------------------------------------------------------------+
| **Username**       | Name of the user (Login credential)                                                |
+--------------------+------------------------------------------------------------------------------------+
| **Email**          | Email of the user                                                                  |
+--------------------+------------------------------------------------------------------------------------+
| **Password**       | Password to login (Login credential)                                               |
+--------------------+------------------------------------------------------------------------------------+
| **First Name**     | First name of the user                                                             |
+--------------------+------------------------------------------------------------------------------------+
| **Last Name**      | Last name of the user                                                              |
+--------------------+------------------------------------------------------------------------------------+
| **Address**        | Address of the user                                                                |
+--------------------+------------------------------------------------------------------------------------+
| **Company**        | Company the user works for                                                         |
+--------------------+------------------------------------------------------------------------------------+
| **Phone**          | Company phone number                                                               |
+--------------------+------------------------------------------------------------------------------------+

When the tab "User Roles" is opened in the Main dialogue box, the user rights can be assigned there.


.. figure:: images/user_roles.png
  :align: center
  :width: 600px
  :name: User roles

  User roles
..

The following settings can be made:

+--------------------+------------------------------------------------------------------------------------+
| **Platform Admin** | If the checkbox is set the new user will have platform admin rights                |
+--------------------+------------------------------------------------------------------------------------+
| **Network Admin**  | Select the VPN Network of which the user will have admin rights                    |
|                    | (if no selection is taken the user won’t be a Network admin).                      |
|                    | See :numref:`User rights table image` for the different user rights.               |
+--------------------+------------------------------------------------------------------------------------+
| **Network User**   | Select the VPN Network the user can access.                                        |
|                    | See :numref:`User rights table image` for the different user rights.               |
+--------------------+------------------------------------------------------------------------------------+

Enabling Two-Factor Authentication (2FA)
===========================================

.. note::
    * 2FA is configured in Keycloak, the identity provider for the Connectivity Suite.

    * It can either be enforced for all users or for individual users only.

    * Keycloak currently supports these authenticator applications:

        * Microsoft Authenticator

        * Google Authenticator

        * FreeOTP


Enforcing 2FA for all users
---------------------------

**Get the Keycloak Admin credentials**

* On the CS server, run this command and make a note of the username and password:

  ``grep KEYCLOAK cs/app/compose.yml``

**Log in to Keycloak**

* In a browser, go to ``https://<cs_domain>/auth/`` .
* Select *Administration Console* and log in using above credentials.

**Select Realm**

* Select *cs* as the Realm in the drop-down menu at the top left:

  .. figure:: images/select_realm.png
      :width: 200px
      :name: select_realm

      Select cs Realm
  ..

**Navigate to Authentication**

* In the left-hand menu, click on *Authentication*.

**Modify the Browser Flow**

* Select the *Browser* flow. This is the flow used when users log in via a web browser.
* Look for the *Browser - Conditional OTP* option, which is typically set to *Conditional* by default. Change this to *Required*.

**Optional: Configure OTP Policy**

* In the left-hand menu, click on *Authentication*.
* Select the tab *Policies*, and there the tab *OTP Policy*.
* Note that the supported authenticator applications may change depending on the OTP policy.

**Set up Mobile Authenticator**

The next time a user logs in, he has to set up a Mobile Authenticator:

.. figure:: images/mobile_authenticator_setup.png
    :width: 400px
    :name: mobile_authenticator_setup

    Mobile Authenticator Setup
..

Enforcing 2FA for individual users
----------------------------------

* Follow the instructions above, but make sure that the *Browser - Conditional OTP* option is set to *Conditional*.
* In the left-hand menu, click on *Users*. Select the desired Username.
* In the field *Required user actions*, select *Configure OTP*, and click *Save*.
* The next time this user logs in, he has to set up a Mobile Authenticator.

Disabling 2FA for a user
^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
    This is not possible if the *Browser - Conditional OTP* option is set to *Required*. This means that you cannot enforce 2FA for all users and then disable it for individual users.

* In the left-hand menu, click on *Users*. Select the desired Username.
* Select the tab *Credentials*.
* Remove the line *Otp* by selecting *Delete* from the corresponding three-dot menu on the right-hand side.

This user can now log in without 2FA.
