Platform Updates
****************

By default, the Connectivity Suite regularly checks for platform updates. The behaviour can be configured in the "Settings".

.. figure:: images/updates_settings.png
    :name: Configure Update Behaviour Image
    :width: 700px

    Configure Update Behaviour
..

The available updates can be found in the "Platform Updates" tab.

.. figure:: images/updates_platform.png
    :name: Available Platform Updates Image
    :width: 700px

    Available Platform Updates Behaviour
..

When "Inform me about Updates" is enabled, a platform update notification will be sent out on a daily basis.

.. figure:: images/updates_notification.png
    :name: Platform Update Notification Image

    Platform Update Notification
..

.. note:: Platform Updates still need to be downloaded and installed manually as described in :ref:`Update & Backup`.
