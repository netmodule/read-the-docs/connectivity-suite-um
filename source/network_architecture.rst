.. _network_architecture:

Network architecture
***********************************

The Connectivity Suite requires two Network Address Blocks that do not overlap with each other or the existing company network address blocks.
The Network Address Blocks:

*	Core Address Block: must be a /20 network (4’096 addresses) used for Connectivity Suite internal services
*	Platform Address block: Can be defined by the user and is used to assign each Device a platform wide unique IP-address. It can be roughly estimated like this:

  Maximum number of Devices X average number of End Devices behind a single Device.
  For example, a /16 network (65’536 addresses) can address 4096 Parent Devices with 16 Child Devices each or 2048 routers with 32 Child Devices each.


.. _Configuring_the_platform_address_block:


System architecture IP overview
===================================

.. figure:: images/networkarchitecturedetail.png
    :name: network-architecture

    Network Architecture
..


Setting the number of VPN Networks
=====================================


During the installation the user must define how many VPN networks he wants to create and how many Devices he wants to assign to a VPN network.
Depending on the use case, this can be very different. See below two examples of different use cases (see :numref:`example_architecture`):

Example 1:
A user who wants to manage all devices in one VPN network would choose the fragmentation so that only one VPN Network is created, but this VPN network can contain a large number of Devices.

Example 2:
A user who, for example, has several customers and creates one VPN Network per customer would probably want to create many VPN Networks, but the VPN Networks only contain a small number of Devices.


To ensure flexiblity the user can fragment the Platform Address Block during the installation according to his use case.


VPN Subnet Pools
-------------------------------------------------

For various reasons (network fragmentation, routing complexity), it would be impractical to set the size for each VPN Network separately.
Instead, different types of VPN Subnet Pools can be defined. three layouts:

**Layout 1: One VPN Subnet Pool**

The whole address space of the Platform Address Block is evenly distributed among the VPN Network Address Blocks, resulting in one VPN Subnet Pool:

.. figure:: images/network_pools_type_1.jpg
  :align: center
  :name: Network type 1

  Network type 1
..


**Layout 2: Two VPN Subnet Pools**

The address space of the Platform Address Block is split in two halves, one for each VPN Subnet Pool type.
Each of these halves' address space is evenly distributed among the VPN Network Address Blocks of the respective type, resulting in two VPN Subnet Pools:

.. figure:: images/network_pools_type_2.jpg
  :align: center
  :name: Network type 2

  Network type 2
..


**Layout 3: Three VPN Subnet Pools**

The Platform Address Block space of the Home Network is split into three parts, one for each VPN Subnet Pool type.
One part contains half of the Platform Address Block space, the other two parts a quarter of the Platform Address Block space each.
Each of these parts Platform Address Block space is evenly distributed among the VPN Network Address Blocks of the respective type, resulting in three VPN Subnet Pools:

.. figure:: images/network_pools_type_3.jpg
  :align: center
  :name: Network type 3

  Network type 3
..


Configure the VPN Subnet Pools
-------------------------------------

Once a layout for the Platform Address Block has been chosen, the VPN Network Address Block types must be configured. Depending on the layout, there are one, two or three of them.

For each VPN Network Address Block type, the following properties must be specified:

* A name to identify the VPN Network Address Block type in the UI.
* The size (number of IP addresses) of a VPN Network Address Block of this type. As the size of the relevant VPN Subnet Pool is fixed, the larger the size of a VPN Network Address Block type is, the less VPN Network Address Blocks of this type will be available. As an example, the :numref:`Network type 3` shows that the same VPN Subnet Pool size can be distributed among 4 VPN Network Address Blocks of type 2 or 16 VPN Network Address Blocks of type 3.
* The default network specifies the VPN Network Address Block of a VPN Network of this type.
* The default number of Child Devices per router is explained in chapter :numref:`setting_end_device_number`.
* The default maximum number of concurrent Service Access users accessing a single VPN Network of this type.


.. _setting_end_device_number:

Setting the number of Child Devices
-------------------------------------------------

As a VPN Network Address Blocks has a fixed amount of IP addresses, the less Child Device addresses are needed, the more Parent Devices can be attached to that VPN Network and vice versa.
If one doesn’t need to access any Child Device at all, then each address of the VPN Network Address Block can be assigned to a Parent Device.

It would be impractical to set the amount of Child Device addresses for each Parent Device individually.
Instead, this amount is configured when a new VPN Network is created (see chapter :numref:`add a network`).
All routers assigned to the same VPN Network share the same amount of accessible Child Devices.

As an illustration, this figure shows two VPN Network Address Blocks of the same size (i.e. they are of the same VPN Network Address Block type), but with a different amount of Child Devices per Parent Device:

.. figure:: images/manydevices.png
  :align: center
  :width: 200px
  :name: Few routers with many End Devices each

  Few routers with many End Devices each per VPN Network Address Block

..


.. figure:: images/few_devices.png
  :align: center
  :width: 200px
  :name: Many routers with few End Devices each

  Many routers with few End Devices each per VPN Network Address Block

..
