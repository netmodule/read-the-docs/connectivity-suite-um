Platform Status
***************

The platform status page gives you a quick overview on how the Connectivity Suite is set up and which certificates are in use.

Network
=======

Platform
--------

The "Platform" section of the "Network" tab explains visually and in a table how you decided to structure the "Platform Address Block" during installation and what's the current usage.

.. figure:: images/platform_status_network_platform.png
    :name: Platform Address Block
    :width: 700px

    Platform Address Block
..

Core
----

The "Core" section of the "Network" tab explains visually and in a table how we structure and use the "Core Address Block" which you had to define during installation.

.. figure:: images/platform_status_network_core.png
    :name: Core Address Block
    :width: 700px

    Core Address Block
..

Network Details Overview
------------------------

When you navigate to the "Network list" and select a network, the "Overview" tab shows visually and in a table the current usage.

.. figure:: images/platform_status_network_overview.png
    :name: Network Overview
    :width: 700px

    Network Overview
..

Certificates
============

Overview
--------

The "Overview" section of the "Certificates" tab provides a quick view on how many certificates are in use and/or are due for renewal or about to expire soon.

.. figure:: images/platform_status_certificates_overview.png
    :name: Certificates Overview
    :width: 700px

    Certificates Overview
..

Certificate Details
-------------------

For each certificate type we show you the details of the certificates. The certificate types covered are:

- Root
- Network
- Device
- Service Access
- Provisioning
- Web

Please note that the "Web" tab is only visible to customers not using "Lets Encrypt" for their HTTPS certificates.

Representative for all certificate types, the following screenshot shows the details for Device Certificates:

.. figure:: images/platform_status_certificates_device.png
    :name: Device Certificates
    :width: 700px

    Device Certificates
..

Links in the grid point to the corresponding entities.
