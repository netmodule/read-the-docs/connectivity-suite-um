NetModule Connectivity Suite
============================
USER MANUAL
-----------

# Documentation Framework

## Sphinx
Please follow the instructions documented [here](http://www.sphinx-doc.org/en/master/usage/quickstart.html).

## Read-The-Docs

## Jenkins
The available Jenkinsfile contains a simple declarative pipeline to create internal HTML files on a daily base.

